import random

list_info = [
    ["Pascal", "ROSE", 43],
    ["Mickaël", "FLEUR", 29],
    ["Henri", "TULIPE", 35],
    ["Michel","FRAMBOISE", 35],
    ["Arthur", "PETALE", 35],
    ["Michel", "POLLEN", 50],
    ["Michel", "FRAMBOISE", 42]
]

def same_info(liste, id) :
    """AI is creating summary for same_info

    Args:
        liste ([str, str, int]): [list of list with a surname, name and age]
        id ([int]): [type of data that you want to extract]

    Returns:
        [str]: [str with the data who's matching]
    """
    #copy la liste de liste pour garder l'originale clean
    ref_liste = liste.copy()
    #parcours la liste
    for ref in ref_liste :
        #creer une liste alt pour transfer la data rechercher
        alt_liste = []
        #comparatif entre la 1ère iteration de notre liste et le reste 
        for personne in ref_liste[ref_liste.index(ref)+1:len(ref_liste)] :
            #si data correspondante, copy dans alt puis remove de ref 
            if ref[id] == personne[id]:
                alt_liste.append(personne)
                ref_liste.remove(personne)
        #alt n'est pas vide, on add la data ref puis on print la data de alt
        if len(alt_liste) != 0 : 
            alt_liste.append(ref)
            txt = " ont des infos identiques"
            for data in alt_liste :  
                txt = "(" + data[0] +", " + data[1] +", " + str(data[2]) + ") et " + txt
            return print(txt)

def recherche_info(ref_liste) : 
    """
    take a list as param, shuffle it and get info 
    Args:
        ref_liste ([str, str, int]): [list of list with a surname, name and age]
    """
    #melange de la liste donner
    random.shuffle(ref_liste)
    #On parcourt la liste
    for personne in ref_liste :
        #Si prénom == Michel ou age>50, on print  
        if personne[0] == "Michel" or personne[2] >= 50 : 
            print("C'est formidable ("+ personne[0], personne[1], str(personne[2]) + ") adore le camping !")
        #Si len prénom et len nom identiques, on print 
        if len(personne[0]) == len(personne[1]) :
            print("C'est formidable " + personne[0], personne[1] + " a " + str(len(personne[0])) + " lettres dans son prénom et nom !")
    
    same_info(ref_liste, 0)
    same_info(ref_liste, 1)
    same_info(ref_liste, 2)

    #go_further
    camping_lover = [ x for x in ref_liste if (x[0] == "Michel" or x[2]>=50)]
    print(camping_lover)

recherche_info(list_info)